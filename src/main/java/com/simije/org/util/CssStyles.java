package com.simije.org.util;

/**
 * Created by anadilo on 8.8.2016.
 */
public class CssStyles {

    public static final String CELL = "-fx-border-color: lightgrey; -fx-border-width: 0 1 1 0; -fx-padding: -3 0 2 -1; -fx-background-color: %s";
    public static final String TILE = "-fx-border-color: lightgrey; -fx-border-width: 1; -fx-background-color: %s";
    public static final String EMPTY = "white";

}
