package com.simije.org.util;

import javafx.concurrent.Task;
import javafx.scene.control.ProgressBar;

import java.util.function.Consumer;

/**
 * Created by anadilo on 15.8.2016.
 */

public class TaskRunner {

    private ProgressBar progress;

    public TaskRunner(ProgressBar progress) {
        this.progress = progress;
    }

    public void run(Consumer<Void> consumer) {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                consumer.accept(null);
                return null;
            }
        };

        task.setOnRunning(e -> progress.setVisible(true));

        task.setOnSucceeded(e -> progress.setVisible(false));
        task.setOnCancelled(e -> progress.setVisible(false));
        task.setOnFailed(e -> progress.setVisible(false));

        new Thread(task).start();
    }

}
