package com.simije.org.util;

import javafx.scene.paint.Color;

/**
 * Created by anadilo on 13.8.2016.
 */

public class FxUtils {

    public static String getHex(Color color) {
        return String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }
}
