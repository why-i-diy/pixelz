package com.simije.org.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anadilo on 12.8.2016.
 */

@Data
public class Settings implements Serializable {

    private List<String> palette = new ArrayList<>();
    private List<String> recents = new ArrayList<>();

    public void addRecent(String recent) {
        if (StringUtils.isBlank(recent) || recents.contains(recent))
            return;

        recents.add(recent);

        if (recents.size() > 7)
            recents.remove(0);
    }
}
