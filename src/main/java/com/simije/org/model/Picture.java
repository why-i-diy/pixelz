package com.simije.org.model;

import com.simije.org.util.CssStyles;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by anadilo on 9.8.2016.
 */

@Data
@NoArgsConstructor
public class Picture implements Serializable {

    public static final String DEFAULT_NAME = "New pixelz";

    private String name;
    private String path;
    private Pixel[] pixels;
    private int rows;
    private int cols;

    public Picture(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

    public static Picture newPicture(int rows, int cols) {
        Picture picture = new Picture(rows, cols);

        picture.setName(DEFAULT_NAME);

        Pixel[] pixels = new Pixel[rows * cols];
        AtomicInteger i = new AtomicInteger(0);

        IntStream.range(0, rows).forEach(r ->
            IntStream.range(0, cols).forEach(c ->
                pixels[i.getAndIncrement()] = new Pixel(CssStyles.EMPTY, r, c)
            )
        );

        picture.setPixels(pixels);

        return picture;
    }

    public boolean isNotSaved() {
        return StringUtils.isBlank(path);
    }

    public void resize(int rows, final int cols) {
        Pixel[] pxs = new Pixel[rows * cols];
        AtomicInteger i = new AtomicInteger(0);

        IntStream.range(0, rows).forEach(r ->
            IntStream.range(0, cols).forEach(c ->
                pxs[i.getAndIncrement()] = new Pixel(getStyle(r, c), r, c)
            )
        );

        this.rows = rows;
        this.cols = cols;

        this.pixels = pxs;
    }

    private String getStyle(int r, int c) {
        for (Pixel px : pixels)
            if (px.getRow() == r && px.getCol() == c)
                return px.getHex();

        return CssStyles.EMPTY;
    }
}
