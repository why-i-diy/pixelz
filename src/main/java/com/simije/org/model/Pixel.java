package com.simije.org.model;

import com.simije.org.component.Cell;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by anadilo on 9.8.2016.
 */

@Data
public class Pixel implements Serializable {

    private int row;
    private int col;
    private String hex;

    public Pixel(String hex, int row, int col) {
        this.hex = hex;
        this.row = row;
        this.col = col;
    }

    public static Pixel createFromCell(Cell cell) {
        return new Pixel(cell.getHex(), cell.getRow(), cell.getCol());
    }
}
