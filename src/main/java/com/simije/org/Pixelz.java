package com.simije.org;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by anadilo on 8.8.2016.
 */

public class Pixelz extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/main.fxml"));
            primaryStage.setTitle("Pixelz");
            primaryStage.setScene(new Scene(root, 900, 500));
            primaryStage.setMaximized(true);
            primaryStage.getIcons().add(new Image("img/logo.png"));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() throws Exception {
        System.exit(0);
    }

}
