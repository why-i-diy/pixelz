package com.simije.org.component;

import com.simije.org.model.Picture;
import javafx.scene.control.ScrollPane;

/**
 * Created by anadilo on 8.8.2016.
 */

public class Tab extends javafx.scene.control.Tab {

    private Grid grid;

    public Tab(Picture picture) {
        super(picture.getName());
        this.grid = new Grid(picture);

        ScrollPane scroll = new ScrollPane(grid);
        scroll.setFitToHeight(false);

        setContent(scroll);
    }

    public Grid getGrid() {
        return grid;
    }

    public Picture getPicture() {
        return grid.getPicture();
    }

    public void refreshName() {
        setText(grid.getPicture().getName());
    }
}
