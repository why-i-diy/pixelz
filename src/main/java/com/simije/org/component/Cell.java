package com.simije.org.component;

import com.simije.org.controller.MainController;
import com.simije.org.model.Pixel;
import com.simije.org.util.CssStyles;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;

/**
 * Created by anadilo on 8.8.2016.
 */

public class Cell extends Label {

    private static final double SIZE = 20;

    private String hex;
    private int col;
    private int row;

    public Cell(String hex, int c, int r) {
        this.hex = hex;
        col = c;
        row = r;

        setStyle(String.format(CssStyles.CELL, hex == null ? CssStyles.EMPTY : hex));
        setPrefSize(SIZE, SIZE);
        setMinSize(5D, 5D);

        setOnMouseClicked(this::clickEvent);
        setOnMouseMoved(this::movedEvent);
    }

    private void movedEvent(MouseEvent event) {
        MainController.statusBar.setValue(toString());

        if (event.isControlDown())
            fill();
        else if (event.isShiftDown())
            erase();
    }

    private void clickEvent(MouseEvent e) {
        if (e.isAltDown()) {
            MainController.statusBar.setValue("Press Escape to close coordinates");
            new Tooltip(toString()).show(this, e.getScreenX(), e.getScreenY());
            return;
        }

        switch (e.getButton()) {
            case PRIMARY:
                fill();
                break;
            case SECONDARY:
                erase();
                break;
            default:
                fill();
        }
    }

    private void fill() {
        setStyle(String.format(CssStyles.CELL, MainController.currentColor.get()));
        this.hex = MainController.currentColor.get();
    }

    public void erase() {
        setStyle(String.format(CssStyles.CELL, CssStyles.EMPTY));
        this.hex = CssStyles.EMPTY;
    }

    public String getHex() {
        return hex;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public static Cell createFromPixel(Pixel px) {
        return new Cell(px.getHex(), px.getCol(), px.getRow());
    }

    @Override
    public String toString() {
        return String.format("%d : %d", col + 1, row + 1);
    }

    public void resize(double size) {
        setPrefSize(size, size);
    }

}