package com.simije.org.component;

import com.simije.org.model.Picture;
import com.simije.org.model.Pixel;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by anadilo on 8.8.2016.
 */

public class Grid extends GridPane {

    private static final double ZOOM_STEP = 5D;
    private static final double MAX_ZOOM = 50D;
    private static final double MIN_ZOOM = 5D;

    private List<Cell> cells = new ArrayList<>();
    private Picture picture;
    private double zoom = 20D;

    public Grid(Picture picture) {
        super();
        this.picture = picture;

        drawGrid(picture);
    }

    public void drawGrid(Picture picture) {
        clear();

        Arrays.asList(picture.getPixels()).stream().forEach(px -> cells.add(Cell.createFromPixel(px)));
        cells.forEach(c -> add(c, c.getCol(), c.getRow()));
    }

    public void clear() {
        cells.clear();
        getChildren().clear();
    }

    public Picture getPicture() {
        picture.setPixels(getPixels());
        return picture;
    }

    public void zoomIn() {
        if (zoom + ZOOM_STEP <= MAX_ZOOM)
            setZoom(zoom + ZOOM_STEP);
    }

    public void zoomOut() {
        if (zoom - ZOOM_STEP >= MIN_ZOOM)
            setZoom(zoom - ZOOM_STEP);
    }

    private void setZoom(double z) {
        this.zoom = z;

        getChildren().forEach(n -> {
            if (n instanceof Cell) {
                ((Cell) n).resize(zoom);
            }
        });
    }

    public Pixel[] getPixels() {
        Pixel[] pixels = new Pixel[cells.size()];
        IntStream.range(0, cells.size()).forEach(i -> pixels[i] = Pixel.createFromCell(cells.get(i)));

        return pixels;
    }

    public void clearGrid() {
        getChildren().forEach(n -> {
            if (n instanceof Cell) {
                ((Cell) n).erase();
            }
        });
    }
}
