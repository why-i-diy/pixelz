package com.simije.org.component;

import com.simije.org.util.CssStyles;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

/**
 * Created by anadilo on 13.8.2016.
 */

public class Tile extends Label {

    private static final double SIZE = 30;
    private String color;

    public Tile(String currentColor) {
        super();
        this.color = currentColor;

        setTooltip(new Tooltip("Right click to remove"));
        setPrefSize(SIZE, SIZE);
        setStyle(String.format(CssStyles.TILE, currentColor));
        setCursor(Cursor.HAND);
    }

    public String getColor() {
        return color;
    }
}
