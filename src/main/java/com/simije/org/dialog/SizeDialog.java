package com.simije.org.dialog;

import com.simije.org.model.Picture;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by anadilo on 16.8.2016.
 */

public class SizeDialog extends Dialog<Pair<Integer, Integer>> {

    private TextField txtWidth = new TextField();
    private TextField txtHeight = new TextField();

    public SizeDialog(int rows, int cols) {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(10));

        grid.add(new Label("Width"), 0, 0);
        grid.add(txtWidth, 1, 0);
        txtWidth.setText(Integer.toString(cols));

        grid.add(new Label("Height"), 0, 1);
        grid.add(txtHeight, 1, 1);
        txtHeight.setText(Integer.toString(rows));

        getDialogPane().setContent(grid);
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        setTitle("New pixelz");

        Platform.runLater(() -> txtWidth.requestFocus());

        setResultConverter(e -> e == ButtonType.OK
                ? Pair.of(Integer.parseInt(txtHeight.getText()), Integer.parseInt(txtWidth.getText()))
                : null);
    }
}
