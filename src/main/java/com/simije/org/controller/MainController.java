package com.simije.org.controller;

import com.simije.org.component.Tab;
import com.simije.org.component.Tile;
import com.simije.org.dialog.SizeDialog;
import com.simije.org.file.PictureIO;
import com.simije.org.file.SettingsIO;
import com.simije.org.model.Picture;
import com.simije.org.model.Settings;
import com.simije.org.util.FxUtils;
import com.simije.org.util.TaskRunner;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.stage.*;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import javax.imageio.ImageIO;
import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

/**
 * Created by anadilo on 8.8.2016.
 */

public class MainController implements Initializable {

    public static StringProperty currentColor = new SimpleStringProperty("white");
    public static StringProperty statusBar = new SimpleStringProperty("");

    @FXML private TabPane tabPane;
    @FXML private ColorPicker picker;
    @FXML private FlowPane palettePane;
    @FXML private ProgressBar progress;
    @FXML private Label coordinates;
    @FXML private AnchorPane welcomePane;

    private PictureIO pictureIO = new PictureIO();
    private SettingsIO settingsIO = new SettingsIO();
    private Settings settings = settingsIO.open();
    private TaskRunner taskRunner;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        settings.getPalette().stream().forEach(p -> addTileToPane(new Tile(p)));
        currentColor.addListener(e -> picker.setValue(Color.web(currentColor.get())));
        picker.valueProperty().addListener(e -> currentColor.setValue(FxUtils.getHex(picker.getValue())));
        statusBar.addListener(e -> coordinates.setText(statusBar.get()));
        showRecent();

        taskRunner = new TaskRunner(progress);
    }

    private void showRecent() {
        final int size = settings.getRecents().size();

        if (size == 0) {
            Hyperlink link = new Hyperlink("Create new...");
            link.setLayoutX(450D);
            link.setLayoutY(80);
            link.setOnAction(this::fileNew);
            welcomePane.getChildren().add(link);
        }

        IntStream.range(0, size).forEach(i -> {
            Hyperlink link = new Hyperlink(settings.getRecents().get(i));
            link.setLayoutX(450D);
            link.setLayoutY(80 + (size - i - 1) * 30);
            link.setOnAction(e -> openPicture(pictureIO.openPath(settings.getRecents().get(i))));
            welcomePane.getChildren().add(link);
        });
    }

    @FXML
    void addToPalette(ActionEvent event) {
        settings.getPalette().add(currentColor.get());
        addTileToPane(new Tile(currentColor.get()));
    }

    private void addTileToPane(Tile tile) {
        tile.setOnMouseClicked(e -> tileClicked(e, tile));
        palettePane.getChildren().add(tile);
        settingsIO.save(settings);
    }

    private void tileClicked(MouseEvent event, Tile tile) {
        if (event.getButton() == MouseButton.PRIMARY)
            currentColor.setValue(tile.getColor());
        else if (event.getButton() == MouseButton.SECONDARY) {
            settings.getPalette().remove(tile.getColor());
            settingsIO.save(settings);
            palettePane.getChildren().remove(tile);
        }
    }

    @FXML
    void fileNew(ActionEvent event) {
        new SizeDialog(30, 50).showAndWait().ifPresent(p ->
            taskRunner.run(t -> {
                Tab tab = new Tab(Picture.newPicture(p.getLeft(), p.getRight()));
                Platform.runLater(() -> tabPane.getTabs().add(tab));
                tabPane.getSelectionModel().select(tab);
            })
        );
    }

    @FXML
    void fileOpen(ActionEvent event) {
        openPicture(pictureIO.open(getWindow()));
    }

    private void openPicture(Picture picture) {
        taskRunner.run(t -> {
            Tab tab = new Tab(picture);
            tab.setTooltip(new Tooltip(picture.getPath()));
            Platform.runLater(() -> tabPane.getTabs().add(tab));
            tabPane.getSelectionModel().select(tab);
        });
    }

    @FXML
    void fileSave(ActionEvent event) {
        pictureIO.save(getWindow(), getSelectedTab().getPicture());
        getSelectedTab().refreshName();
        statusBar.setValue("Pixelz saved");
        settings.addRecent(getSelectedTab().getPicture().getPath());
        settingsIO.save(settings);
    }

    @FXML
    void fileSaveAs(ActionEvent event) {
        pictureIO.saveAs(getWindow(), getSelectedTab().getPicture());
        getSelectedTab().refreshName();
        statusBar.setValue("Pixelz saved");
        settings.addRecent(getSelectedTab().getPicture().getPath());
        settingsIO.save(settings);
    }

    @FXML
    void fileClose(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void zoomIn(ActionEvent event) {
        getSelectedTab().getGrid().zoomIn();
    }

    @FXML
    void zoomOut(ActionEvent event) {
        getSelectedTab().getGrid().zoomOut();
    }

    @FXML
    void closeTab(ActionEvent event) {
        tabPane.getTabs().remove(getSelectedTab());
    }

    @FXML
    void closeAllTabs(ActionEvent event) {
        tabPane.getTabs().clear();
    }

    @FXML
    @SneakyThrows
    void showAbout(ActionEvent event) {
        Stage stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("fxml/about.fxml"));
        stage.setTitle("About Pixelz");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }

    @FXML
    void clearGrid(ActionEvent event) {
        getSelectedTab().getGrid().clearGrid();
    }

    @FXML
    void resizeGrid(ActionEvent event) {
        Picture picture = getSelectedTab().getGrid().getPicture();
        new SizeDialog(picture.getRows(), picture.getCols()).showAndWait().ifPresent(s -> {
            tabPane.getTabs().remove(tabPane.getSelectionModel().getSelectedIndex());
            picture.resize(s.getLeft(), s.getRight());
            openPicture(picture);
        });
    }

    @FXML
    @SneakyThrows
    void export(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("png", "*.png"),
                new FileChooser.ExtensionFilter("jpg", "*.jpg"));
        chooser.setInitialFileName(StringUtils.substringBefore(getSelectedTab().getPicture().getName(), "."));

        File file = chooser.showSaveDialog(getWindow());
        if (file == null)
            return;

        WritableImage img = getSelectedTab().getGrid().snapshot(new SnapshotParameters(), null);
        ImageIO.write(SwingFXUtils.fromFXImage(img, null), chooser.getSelectedExtensionFilter().getDescription(), file);
    }

    private Tab getSelectedTab() {
        return (Tab) tabPane.getSelectionModel().getSelectedItem();
    }

    private Window getWindow() {
        return tabPane.getScene().getWindow();
    }

}
