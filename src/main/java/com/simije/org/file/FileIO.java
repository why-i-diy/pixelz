package com.simije.org.file;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import lombok.Cleanup;

import java.io.*;

/**
 * Created by anadilo on 12.8.2016.
 */

abstract class FileIO<T> {

    void save(T t, String path) {
        try (FileOutputStream fos = new FileOutputStream(path)) {
            @Cleanup ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(t);
        } catch (Exception e) {
            new Alert(Alert.AlertType.ERROR, "Error saving file.", ButtonType.CLOSE).showAndWait();
            e.printStackTrace();
        }
    }

    T open(String path, boolean alert) {
        try (FileInputStream fis = new FileInputStream(path)) {
            @Cleanup ObjectInputStream ois = new ObjectInputStream(fis);
            return (T) ois.readObject();
        } catch (FileNotFoundException fe) {
            if (alert)
                new Alert(Alert.AlertType.INFORMATION, "File does not exist.", ButtonType.OK).showAndWait();
            fe.printStackTrace();
            return null;
        } catch (Exception e) {
            if (alert)
                new Alert(Alert.AlertType.INFORMATION, "Error reading file.", ButtonType.OK).showAndWait();
            e.printStackTrace();
            return null;
        }
    }


}
