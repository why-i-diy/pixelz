package com.simije.org.file;

import com.simije.org.model.Picture;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import java.io.File;

/**
 * Created by anadilo on 9.8.2016.
 */

public class PictureIO extends FileIO<Picture> {

    public void save(Window window, Picture picture) {
        if (picture.isNotSaved())
            saveAs(window, picture);
        else
            save(picture, picture.getPath());
    }

    public void saveAs(Window window, Picture picture) {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Pixelz files", "*.pxz"));
        chooser.setInitialFileName(picture.getName());

        File file = chooser.showSaveDialog(window);

        if (file == null)
            return;

        picture.setName(file.getName());
        picture.setPath(file.getPath());

        save(picture, picture.getPath());
    }

    public Picture open(Window window) {
        FileChooser chooser = new FileChooser();
        chooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Pixelz files", "*.pxz"));

        File file = chooser.showOpenDialog(window);

        if (file == null)
            return null;

        return open(file.getPath(), true);
    }

    public Picture openPath(String path) {
        return open(path, true);
    }

}
