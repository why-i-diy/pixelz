package com.simije.org.file;

import com.simije.org.model.Settings;

/**
 * Created by anadilo on 12.8.2016.
 */

public class SettingsIO extends FileIO<Settings> {

    private static final String PATH = System.getProperty("user.dir") + "\\.settings";

    public void save(Settings settings) {
        save(settings, PATH);
    }

    public Settings open() {
        Settings settings = open(PATH, false);

        if (settings == null) {
            save(new Settings());
            return open(PATH, false);
        }

        return settings;
    }
}
